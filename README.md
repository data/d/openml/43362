# OpenML dataset: Netflix_movies

https://www.openml.org/d/43362

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Content
This Dataset Contains informations about Netflix movies, and how the Netflix website and use it to give me recommendations about movies of the genre that I prefer.
Here, I used Netflix's API to get data and I did EDA ,cleaning and Visualization to get start the  recommendationssystems machine learning.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43362) of an [OpenML dataset](https://www.openml.org/d/43362). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43362/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43362/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43362/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

